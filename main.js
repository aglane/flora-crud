import Vue from 'vue';
import Vuetify from 'vuetify';
import Vuex from 'vuex';
import { ValidationProvider } from 'vee-validate';
import VueLazyload from 'vue-lazyload';
import JsonExcel from 'vue-json-excel';
import store from './store/store';
import App from './App.vue';
import router from './router';

const opts = {
  theme: {
    dark: false,
    themes: {
      light: {
        primary: '#00274c',
        secondary: '#b0bec5',
        accent: '#8c9eff',
        error: '#b71c1c',
      },
      dark: {
        primary: '#00274c',
        secondary: '#b0bec5',
        accent: '#8c9eff',
        error: '#b71c1c',
      },
    },
  },
  icons: {
    iconfont: 'mdiSvg',
  },
};

Vue.use(Vuetify);

Vue.use(Vuex);

Vue.use(VueLazyload, {
  preLoad: 1.3,
  error: 'dist/error.png',
  loading: 'dist/loading.gif',
  lazyComponent: true,
  observer: true,
});

Vue.component('ValidationProvider', ValidationProvider);

Vue.component('downloadExcel', JsonExcel);

Vue.config.productionTip = false;

/* eslint-disable no-new */

const vm = new Vue({
  router,
  store,
  vuetify: new Vuetify(opts),
  render: h => h(App),
}).$mount('#app');

global.vm = vm;

/* const vm = new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
  vuetify: new Vuetify(opts),
});
*/
