import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    isSubmitted: false,
  },
  mutations: {
    change(state, isSubmitted){
      state.isSubmitted = true;
    },
  },
  actions: {
    isSubmitted: state => state.isSubmitted,
  },
  modules: {
  },
});
