<template>
  <v-container fluid class="ma-0 pa-0">
    <NavTop />
    <v-row class="mt-8"></v-row>
    <v-content class="mt-12">
      <v-row class="mt-12">
        <v-col lg="3" md="1" cols="1">
          <!-- Fills extra space in the row -->
        </v-col>
        <v-col cols="10" lg="8" sm="10" class="mt-12 text-justify">
          <p :class="{'display-1': $vuetify.breakpoint.lgAndUp, 'headline': $vuetify.breakpoint.mdAndDown}">Information</p>
          <v-divider></v-divider>
        </v-col>
      </v-row>
      <v-row no-gutters>
        <v-col lg="3" md="1" cols="1">
          <NavDrawer />
        </v-col>
        <v-col cols="10" lg="8" sm="10" class="mt-4 text-justify">
          <p>The information presented in this website is updated from the three volumes of <em>Michigan Flora</em> in three main ways.</p>

          <p>First, the advances in understanding of plant relationships result in the need to change a number of names to reflect this new information. These advances have been tremendous in the last couple of decades, driven by our ability to directly access the genetic code, as well as the development of computational tools and computer power sufficient to analyze such vast amounts of data. This includes reorganization of genera, most especially in difficult families such as Asteraceae, Brassicaceae, Cyperaceae, and Poaceae, as well as circumscriptions of some plant families, following, with minor exceptions the <a href="http://www.mobot.org/MOBOT/research/APweb/welcome.html">Angiosperm Phylogeny Group</a>. In addition, there have been a modest number of routine nomenclatural changes required by the International Code of Botanical Nomenclature. <router-link to="/family-changes">Genera in <em>Michigan Flora</em> whose family assignments have changed</router-link> summarizes the family reassignments in tabular form.</p>

          <p>Second, there have been a large number of species added to the flora. The total herein of seed plants (as of the latest revision in 2016; see the section "Additions since the Field Manual of Michigan Flora (2012)" below) is 2775&nbsp;species, which represents a net increase of 310 species, or nearly 13%, over the 2465 species mapped in the three volumes of <em>Michigan Flora</em>. Adding the 108 ferns and other spore bearing vascular plants to this gives a total of 2883 species in this website. The total number of entities treated, however, is 2889&nbsp;because 6 species have dual entries, for native and alien elements. These six species are <em>Allium schoenoprasum</em>, <em>Echinacea purpurea</em>, <em>Myrica pensylvanica</em>, <em>Phragmites australis</em>, <em>Symphoricarpos albus</em>, and <em>Veronica beccabunga</em>. The great majority of these 310 additions, 291&nbsp;or ca. 10% of the total flora, are species newly discovered to be part of the state flora (or in a very few cases confirmed with more ample material). Most of these additions (246 or nearly 80% of the additions) sadly represent aliens, the most dynamic element in the flora. Other additional species are the result of better understanding of difficult groups (often helped by ample new collections), or in rare cases re-identification of older collections. In a few instances, species have disappeared due to re-identification or “lumping” due to better understanding of variation. All the name changes, additions, and deletions to the three volumes of <em>Michigan Flora</em> are listed in <router-link to="/flora-changes"> Additions and name changes to <em>Michigan Flora</em></router-link>, with highlights also noted in the next section.&nbsp;In adding species, we have tried to retain the basic structure of the <em>Michigan Flora</em> keys, where possible, and in cases where keys were dramatically altered, the key construction principles used in <em>Michigan Flora</em> were followed. While the many changes in names may be frustrating, take heart that the characters used to distinguish a species remain the same, regardless of the name used!</p>

          <p>Third, the maps are all regularly updated, following the firm precedent set in <em>Michigan Flora</em> that the maps are based only on specimens examined. Since the publication of Volume 1 of <em>Michigan Flora</em> on June 30, 1972, about 55,000 Michigan specimens have been added to the University of Michigan Herbarium alone, thanks to the numerous devoted collectors who have worked to document Michigan’s plants. In many cases, this vast trove of new information dramatically alters the distributions of species, especially rapidly spreading aliens. When native species have not been collected for a long period of time (usually several decades) so as to suggest they may be becoming rare, we have tried to note that as well. For new discoveries, we have tried to note the year and county of the find, as well as the collector. Infraspecific entities, varieties and subspecies, are normally only discussed when more than one is present in Michigan, and are not mapped.</p>

          <h2>Additions since the Field Manual of Michigan Flora (2012)</h2>

          <p>Species added to the website since the publication of the <em>Field Manual of Michigan Flora</em> in 2012 include two new native plant discoveries, <em>Aronia arbutifolia</em> and <em>Artemisia serrata</em>, plus, sad to say, 25 aliens added in 2012: <em>Acalypha ostryifolia</em>, <em>Agastache rugosa</em>, <em>Anemone blanda</em>, <em>Aralia spinosa</em>, <em>Chenopodium polyspermum</em>, <em>Cyperus iria</em>, <em>Eranthis hyemalis</em>, <em>Eriochloa contracta</em>, <em>Euphorbia serpens</em>, <em>Exochorda racemosa</em>, <em>Geranium sibiricum</em>, <em>Hibiscus syriacus</em>, <em>Hydrocotyle ranunculoides</em>, <em>Iris ×vinicolor</em>, <em>Magnolia acuminata</em>, <em>Microstegium vimineum</em>,&nbsp;<em>Molinia caerulea</em>, <em>Napaea dioica</em>, <em>Nicotiana alata</em>, <em>Phyllostachys nigra</em>, <em>Pinus rigida</em>, <em>Spiraea nipponica</em>, <em>Spiraea prunifolia</em>, <em>Tripidium ravennae</em>, and <em>Viburnum sieboldii.</em></p>

          <p>Six species of aliens were added in 2013:&nbsp;<em>Elsholtzia ciliata, Hedera helix, Myriophyllum aquaticum, Picea pungens, Rubus phoenicolasius</em>, and <em>Schoenoplectiella mucronata.&nbsp;</em></p>

          <p>In 2014, a March review of <em>Senecio </em>resulted in adding two alien species, <em>S</em>. <em>vernalis</em> and <em>S</em>. <em>viscosus</em>, plus recognition of the genus <em>Jacobaea. </em>In May, the alien <em>Jasione montana </em>was added through the discovery of an overlooked herbarium specimen at MICH. In June, 2014, three additional <em>Juncus</em> species were added, based on herbarium work at MICH by Peter Zika. The native <em>Juncus dichotomus</em> (discussed from Kalamazoo Co. in Michigan Flora, Vol. 1, under <em>J. tenuis</em>) was confirmed from Michigan, and an additional specimen was discovered from Berrien Co., and the introduced <em>J. ranarius</em>&nbsp;was confirmed from an older Farwell collection. In addition, <em>J. pylaei </em>was split from <em>J. effusus</em>. The rest of the summer brought five additional species, <em>Allium carinatum</em>, <em>Chelone lyonii</em>,&nbsp;<em>Luzula luzuloides</em>, <em>Rubus parvifolius</em>, and <em>Tanacetum coccineum</em>, all alien.</p>

          <p>The year 2015 brought another eleven additions&nbsp;to the list, all alien:&nbsp;<em>Alchemilla mollis</em>,<em>&nbsp;Bothriochloa laguroides</em>,&nbsp;<em>Colchicum autumnale</em>,&nbsp;<em>Dioscorea polystachya</em>,&nbsp;<em>Eichhornia crassipes</em>,&nbsp;<em>Galanthus elwesii</em>,&nbsp;<em>Galium pedemontanum</em>, <em>Juncus validus</em>,&nbsp;<em>Nicotiana tabacum</em>,&nbsp;<em>Nymphoides peltata</em>, and&nbsp;<em>Pistia stratiotes</em>.&nbsp;These represent mostly new discoveries, plus an acknowledgement that some tropical species grown in water gardens are sufficiently commonly re-introduced every summer, and sufficiently fast growing, that they require being added to the flora.&nbsp;The <em>Nicotiana</em> represents an unusual case, however, having been collected from a “roadside” by the First Survey in 1837.&nbsp;</p>

          <p>In 2016, 5 new plants were collected:&nbsp;<em>Ampelamus laevis</em>, <em>Crocosmia</em> ×<em>crocosmiiflora</em>, another one of the few hybrid taxa adopted fully and mapped,<em> Liquidambar styraciflua</em>, <em>Microthlaspi perfoliatum</em>, and <em>Rhamnus davurica</em>, all alien. An additional species was added to the ferns by the recognition of <em>Botrychium tenebrosum</em>, split from <em>B. simplex</em>.The most remarkable addition was the description of a new woody plant, <em>Hypericum swinkianum</em>, known&nbsp;from several scattered sites in Michigan.</p>

          <h2>Farwell Collections</h2>

          <p>Michigan is unique in having had a brilliant and productive but also eccentric and disorganized botanist devote a long career to the flora of the State.&nbsp;Oliver A. Farwell (1867–1944)&nbsp;made over 12,000 collections in Michigan, more than any other collector except Edward G. Voss. Most of these were from the Keweenaw Peninsula or southeastern&nbsp;Michigan, especially Macomb, Monroe, Oakland, and Wayne Counties; both regions well known for having interesting species at their range limits or disjunct. Unfortunately, besides being a prolific collector and having a talent for finding rarities and unusual aliens, Farwell also labeled his early specimens in particular long after collecting and had “known unmethodical habits in preserving and labelling his specimens” (Hermann, 1951). In addition, Farwell had a penchant for re-numbering his collections, and intercalating additional collections into his series through fractional numbers ("½, ⅓," etc.).&nbsp;</p>

          <p>Plant distributions in Michigan and the Great Lakes region were poorly known in the earlier periods when Farwell was active, but even so, Farwell seemed to have had little interest in the distribution of plants; “…his collections of plants with strikingly disjunct ranges seems to have stirred no geographical interest on his part or concern with the difficult problems of historical phytogeography” (McVaugh, Cain, &amp; Hagenah, 1953). Thus, unusual disjunct records did not seem to warrant his special attention and double-checking. Also, though he did not exchange specimens extensively with other botanists, he did receive some material from outside of Michigan. Specimens in his herbarium were evidently stored for long periods in bundles, unmounted and scantily labeled and, given Farwell’s habits, there was ample opportunity for mix-ups.&nbsp;</p>

          <p>Warning signs for collections that are suspect include having vague and generalized data (no habitat, locality often only County), having one of the notorious "½" numbers, having dates that are inappropriate for the condition of the plant, having disjunct distributions that seem inappropriate (not fitting with known patterns), and specimens belonging to variants of the species not otherwise known from the area.</p>

          <p>Some early collections by Farwell also show clearer symptoms of mix-ups. As Drife &amp; Drife (1990) note, some of the truly bizarre Farwell records cluster on the same days. For example, he apparently collected on the Keweenaw, on September 20, 1888, <em>Asplenium montanum</em>, <em>A. ruta-muraria</em>, <em>Dennstaedtia punctilobula</em>, <em>Phegopteris hexagonaptera</em>, and a number of southern <em>Carex</em>, including <em>C.</em><em> jamesii</em>, <em>C. davisii</em>, <em>C. squarrosa</em>, and <em>C. virescens</em>. Obviously inconceivable, even Farwell knew something was wrong, noting for the <em>Carex</em> (which were in young fruit – also impossible in late September!) ”Probably introduced in imported hay… as I found them only in the season of 1888.” See the discussion under&nbsp;<em>Carex jamesii</em>&nbsp;in Voss&nbsp;(1972, pg. 285). Of course, the two&nbsp;<em>Asplenium</em> were not in imported hay! Farwell probably had a small bundle or bundles of specimens of southern species, presumably received on exchange, whose origin he mixed up long after the fact. This same&nbsp;circumstance&nbsp;is seen with other unacceptable Farwell&nbsp;records (e.g., he allegedly collected both <em>Polystichum acrostichoides</em> and <em>Woodsia obtusa</em> on the Keweenaw on August 20, 1889).&nbsp;</p>

          <p>Though errors are less frequent than with Farwell, after more than two centuries of collecting in Michigan, occasional mix-ups are unavoidable, and these&nbsp;criteria also apply to other collectors. Thus, there are a number of species throughout this flora where we do not accept particular Farwell collections (and occasionally those of other collectors) that are surely the result of data mix-ups. These are noted in the text, often with a brief commentary. In some cases, we tentatively accept the record, but mention that it is problematic. Farwell’s collections also figure in some of the species noted below; where we felt evidence was insufficiently strong to formally include them in the State’s flora.</p>

          <h2>Species Not Accepted As Members Of The Wild Flora</h2>

          <p>This list is a mixture of specimens that were certainly collected where the labels said, but doubt remains about their true status at the site – whether planted or not or whether truly escaped or not – plus a few specimens whose attribution to Michigan is likely the result of a label error.</p>

          <p>&nbsp;</p>

          <ul>
            <li><strong><em>Aquilegia&nbsp;chrysantha</em></strong>&nbsp; This species, or a hybrid of it, collected once along a roadside in 1959 in Keweenaw Co. (See&nbsp;<em>Aquilegia vulgaris</em>)&nbsp;</li>
            <li><strong><em>Asplenium montanum</em></strong>&nbsp; A Farwell specimen of this species was&nbsp;purported to have been collected in the Keweenaw Peninsula, but Drife &amp; Drife (1990) concluded that it was clearly the result of a labeling error. (See&nbsp;<em>Asplenium</em>)&nbsp;</li>
            <li><strong><em>Baptisia australis</em></strong>&nbsp;Collected on the campus of Western Michigan University in Kalamazoo, where possibly planted, and also as persisting at a prairie planting in Lenawee Co. (See&nbsp;<em>Baptisia</em>)</li>
            <li><strong><em>Catalpa bignonioides</em></strong>&nbsp;Collected along a road in Kalamazoo Co. (See&nbsp;<em>Catalpa</em>)</li>
            <li><strong><em>Chaenomeles japonica</em></strong> Collected in 1891 in Washtenaw Co., but without clear indication of whether planted or escaped (See <em>Chaenomeles</em>)</li>
            <li><strong><em>Clarkia unguiculata</em></strong> Collected in 2009 as "persisting" from a "wildflower" planting in Ontonagon Co. (See Onagraceae)</li>
            <li><strong><em>Coreopsis nuecensis</em></strong>&nbsp;Collected in a field (where possibly planted)&nbsp;by a railroad at Flat Rock, Wayne Co. (See&nbsp;<em>Coreopsis</em>)</li>
            <li><strong><em>Cornus mas</em></strong>&nbsp;Collected along a road beside a park in Washtenaw Co. (See&nbsp;<em>Cornus</em>)</li>
            <li><strong><em>Epilobium obscurum</em></strong> Collected in 1927 at a private greenhouse and gardens, but not definitely as an out-of-doors weed. (See <em>Epilobium</em>)&nbsp;</li>
            <li><strong><em>Fagopyrum tataricum</em></strong> Collected long ago (no date) in Shiawassee Co., perhaps only from a planting. (See <em>Fagopyrum esculentum</em>)</li>
            <li><strong><em>Fallopia baldschuanica</em></strong><em>&nbsp;</em>Collected, but perhaps only persisting from a planting, in Lenawee Co. (See <em>Fallopia scandens</em>)</li>
            <li><strong><em>Fragaria&nbsp;</em></strong><strong>×<em>ananassa</em></strong>&nbsp;Collected as an escape or probably more likely only persisting for a time after cultivation in Antrim, Kalamazoo, Keweenaw, and Wayne Cos. (See <em>Fragaria virginiana</em>)</li>
            <li><strong><em>Geranium pratense</em></strong>&nbsp;Collected along a driveway in Washtenaw Co. (See&nbsp;<em>Geranium maculatum</em>)</li>
            <li><strong><em>Ginkgo biloba</em></strong>&nbsp;Collected as small seedlings near parent trees in Kalamazoo and Lenawee Cos. (Not treated)</li>
            <li><strong><em>Hyacinthus orientalis</em></strong> Collected persisting where dumped with garden refuse in Alpena Co. (See Hyacinthaceae)</li>
            <li><strong><em>Lactuca sativa</em></strong>&nbsp;Collected, possibly out of cultivation, in Wayne Co. (See&nbsp;<em>Lactuca</em>)</li>
            <li><strong><em>Linum lewisii</em></strong> Collected in Washtenaw and Wayne Cos., perhaps merely cultivated or persisting briefly on cultivated soil. (See&nbsp;<em>Linum perenne</em>)</li>
            <li><strong><em>Narcissus ×medioluteus</em></strong> Collected as a&nbsp;“presumed escape” in Leelanau Co. (See&nbsp;<em>Narcissus</em>)</li>
            <li><strong><em>Oenothera nutans</em></strong> Three old specimens (Ingham, Washtenaw, and Genesee Cos., 1879–1911) in poor condition and/or with scanty data represent this large flowered species in Michigan, but even if not simply cultivated, the specimens apparently represent non-persistent waifs. (See <em>Oenothera</em>)</li>
            <li><strong><em>Paeonia lactiflora</em></strong><strong>&nbsp;</strong>Collected as persisting clumps or dumped waifs in Crawford and Lenawee Cos. (See Ranunculaceae)</li>
            <li><strong><em>Papaver argemone</em></strong> Collected in 1896 in a crimson clover field in Grand Traverse Co., presumably as a seed contaminant. (See <em>Papaver</em>)</li>
            <li><strong><em>Pilea microphylla</em></strong> Collected in 1958 in a nursery in Ingham Co. (See <em>Pilea</em>)</li>
            <li><strong><em>Platycodon grandiflorus</em></strong>&nbsp;Collected as long persisting in St. Clair Co. (See Campanulaceae)</li>
            <li><strong><em>Prunus cerasifera</em></strong>&nbsp;Collected as “possibly planted” in Cheboygan Co. (See&nbsp;<em>Prunus angustifolia</em>)</li>
            <li><strong><em>Prunus glandulosa</em></strong><strong>&nbsp;</strong>Collected as “spreading somewhat” at an old planting in Benzie Co. (See&nbsp;<em>Prunus</em>)</li>
            <li><strong><em>Ribes alpinum</em></strong>&nbsp;Collected as "long persistent" in Baraga Co. (See&nbsp;<em>Ribes</em>)</li>
            <li><strong><em>Robinia neomexicana</em></strong>&nbsp;Collected as spreading by suckers from planted trees in Houghton Co. (See <em>Robinia viscosa</em>)</li>
            <li><strong><em>Rosa damascena</em></strong> Collected by Farwell as "persistent" around the cellar remains of an old house in Wayne Co. (See <em>Rosa</em>)&nbsp;</li>
            <li><em><strong>Salvia sclarea</strong> </em>Collected ca. 1910 from Suttons Bay, Leelanau Co., but with virtually no data and perhaps only from cultivation. (See <em>Salvia</em>)&nbsp;</li>
            <li><em><strong>Schoenoplectus heterochaetus </strong></em>is represented by two specimens labeled as having been collected at Lansing (or the Agricultural College), Ingham Co. (without collector, in 1871, MSC), and was included in the <em>Field Manual of Michigan Flora</em>. As these two sheets also have neither habitat nor a&nbsp;specific date, and the species has never been seen elsewhere in the State, it seems likely that there was a label mix-up. (See <em>Schoenoplectus</em>)</li>
            <li><strong><em>Sedum sediforme</em></strong>&nbsp;Collected&nbsp;as “spreading in a garden”&nbsp;in 1974 in Ann Arbor, Washtenaw Co. (See <em>Sedum</em>)</li>
            <li><strong><em>Sempervivum tectorum</em></strong> Collected presumably where garden refuse was dumped along roadsides, but probably never actually established,&nbsp;in&nbsp;Barry and Benzie Cos. (See Crassulaceae)</li>
            <li><em>S<strong>ilene flos-cuculi</strong></em> Collected in 1885 by an unknown collector from "Farm Flats," perhaps from cultivated plants, in Lansing, Ingham Co. (See <em>Silene</em>)</li>
            <li><em><strong>Silene pendula</strong> </em>Collected&nbsp;as “Escaped from flower garden”&nbsp;in 1887&nbsp;at Lansing, Ingham Co. (See <em>Silene</em>)</li>
            <li><strong><em>Symphyotrichum prenanthoides </em></strong>&nbsp;Collected in Keweenaw Co., in 1890; so far out of range as to suggest a labeling error. (See <em>Symphyotrichum)</em></li>
            <li><strong><em>Symphyotrichum patens</em></strong><em> </em>A sheet simply labeled Lansing is surely a label error. (See <em>Symphyotrichum</em>)</li>
            <li><strong><em><strong><em>Tagetes erecta</em></strong> </em></strong>Collected in Chippewa Co., in 1831 by&nbsp;Douglass Houghton, but without indication of whether wild or cultivated. (See&nbsp;<em><em>Dyssodia</em></em>)</li>
            <li><strong><em><strong><em>Tulipa&nbsp;×gesneriana</em></strong>&nbsp;</em></strong>Specimens have been collected, presumably persisting from cultivation, but perhaps as dumped waifs,&nbsp;in Houghton, Keweenaw, Wayne, and Washtenaw Cos. (See Liliaceae)</li>
            <li><strong><em><strong><em>Vernonia fasciculata</em></strong>&nbsp;</em></strong>Collected at a wetland mitigation site in Wayne Co. in 2004, but likely part of the plantings. (See <em>Vernonia</em>)</li>
            <li><strong><em><em>Viburnum lantanoides</em> </em></strong>Collected allegedly at Pentwater (Oceana Co.) in 1938, but with scanty, generic label data, perhaps suggesting a data mix-up. (See <em><em>Viburnum</em></em>) <strong><em>&nbsp;</em></strong></li>
          </ul>
        </v-col>
      </v-row>
    </v-content>
    <TheFooter />
  </v-container>
</template>

<script>
import NavTop from '@/components/NavTop.vue';
import NavDrawer from '@/components/NavDrawer.vue';
import TheFooter from '@/components/TheFooter.vue';

export default {
  name: 'TheInformation',
  components: {
    NavTop,
    NavDrawer,
    TheFooter,
  },
};
</script>

<style scoped>

</style>
